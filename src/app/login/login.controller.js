(function() {
  'use strict';

  angular
    .module('capaegisAssignment')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController(parallaxHelper) {
    var vm = this;

    vm.showHelloWorld = false;
    vm.showError = false;
    vm.username;
    vm.password;
    vm.login = login;

    function login() {
      if (!vm.username || !vm.password || vm.username.length < 1 || vm.password.length < 1) {
        vm.showError = true
      } else {
        vm.showHelloWorld = true;
        vm.showError = false;
      }
    }
  }
})();
