(function() {
  'use strict';

  angular
    .module('capaegisAssignment')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
