(function() {
  'use strict';

  angular
    .module('capaegisAssignment')
    .directive('textMalarkey', textMalarkey);

  /** @ngInject */
  function textMalarkey(malarkey) {
    var directive = {
      restrict: 'E',
      scope: {
        values: '='
      },
      template: '&nbsp;',
      link: linkFunc,
      controller: MalarkeyController,
      controllerAs: 'vm'
    };

    return directive;

    function linkFunc(scope, el, attr, vm) {
      var watcher;
      var typist = malarkey(el[0], {
        typeSpeed: 400,
        deleteSpeed: 300,
        pauseDelay: 800,
        loop: true,
        postfix: ' '
      });

      el.addClass('text-malarkey');

      angular.forEach(scope.values, function(value) {
        typist.type(value)
          .pause()
          .delete();
      });
    }

    /** @ngInject */
    function MalarkeyController($log) {
      var vm = this;

    }

  }

})();
