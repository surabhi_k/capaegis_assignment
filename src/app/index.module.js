(function() {
  'use strict';

  angular
    .module('capaegisAssignment', ['ngResource', 'ui.router', 'ngMaterial', 'toastr', 'duParallax', 'ngAnimate']);

})();
